��          <      \       p      q      x   O   �     �      �     �  R   �                   Accept Parent Agreement Your parents must login first and accept the Parent Agreement so you can login. Project-Id-Version: Parent Agreement plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:15+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Aceptar Acuerdo Padres Sus padres deben entrar primero y aceptar el Acuerdo Padres para que pueda entrar. 